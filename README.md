## su3_bench: Lattice QCD SU(3) matrix-matrix multiply microbenchmark  
Su3_bench has moved to the NERSC Proxies Project: https://gitlab.com/NERSC/nersc-proxies/su3_bench

### Contact info
Doug Doerfler
dwdoerf@lbl.gov
